package dev.sieber.gifs.ui.search.view

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import dev.sieber.gifs.R
import dev.sieber.gifs.databinding.ItemGifBinding
import dev.sieber.gifs.model.Result
import dev.sieber.gifs.utils.DeviceUtils
import dev.sieber.gifs.utils.GifUtils
import org.jetbrains.anko.sdk25.coroutines.onClick
import java.util.Random

class GifViewHolder(private var binding: ItemGifBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(result: Result, gifClickListener: SearchClickListener) {
        with(binding) {
            this.gifResult = result
            executePendingBindings()

            val imageWidth = DeviceUtils.getDeviceWidth(imageview.context) / 2

            //calc height
            result.media?.let {
                val orginal = it[0].gif
                orginal?.let { }
                val height = orginal!!.dims!![1] * imageWidth / orginal.dims!![0]

                imageview.layoutParams = ViewGroup.LayoutParams(imageWidth, height)
                imageview.setBackgroundColor(ContextCompat.getColor(imageview.context, getRandomBackgroundColor()))

                val image = it[0].tinygif
                GifUtils.loadGif(image!!.url, imageview)

                itemView.onClick {
                    gifClickListener.onGifClickListener(result)
                }
            }
        }
    }

    private fun getRandomBackgroundColor(): Int {
        val backgroundColors = intArrayOf(
            R.color.item_backgground_1,
            R.color.item_backgground_2,
            R.color.item_backgground_3,
            R.color.item_backgground_4,
            R.color.item_backgground_5,
            R.color.item_backgground_6,
            R.color.item_backgground_7,
            R.color.item_backgground_8
        )
        val position = Random().nextInt(backgroundColors.size - 1 - 0 + 1) + 0
        return backgroundColors[position]
    }

}