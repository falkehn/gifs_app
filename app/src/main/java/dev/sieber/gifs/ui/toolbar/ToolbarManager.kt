package dev.sieber.gifs.ui.toolbar

import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.Observer
import android.databinding.DataBindingUtil
import android.support.design.widget.AppBarLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v4.content.ContextCompat
import android.support.v7.widget.SearchView
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import dev.sieber.gifs.R
import dev.sieber.gifs.databinding.ViewToolbarBinding
import kotlinx.android.synthetic.main.view_toolbar.view.toolbar


/**
 * A Utility Class that builds the Toolbar using the [ToolbarModel].
 *
 * @param lifecycleOwner the [FragmentActivity] or the [Fragment] where the Toolbar will be displayed.
 * @param container the View-Container in which the [com.lidl.eci.R.layout.view_toolbar] layout is located.
 * @param toolbarModel the model how the Toolbar should be build
 * @param translationUtils Utils to automatically translate the [MenuItem] text
 */
class ToolbarManager constructor(
    private var lifecycleOwner: LifecycleOwner,
    private var container: View,
    private var toolbarModel: ToolbarModel
) {

    /**
     * Call this Function in the [FragmentActivity.onCreate] or [Fragment.onViewCreated] to start the Toolbar.
     */
    fun prepareToolbar() {
        if (toolbarModel.resId != ToolbarModel.NO_TOOLBAR) {
            // find the Toolbar-View
            val toolbarLayout = container.findViewById<View>(toolbarModel.resId)
            // setup the databinding
            var toolbarDatabinding = DataBindingUtil.getBinding<ViewToolbarBinding>(toolbarLayout)
            if (toolbarDatabinding == null) {
                toolbarDatabinding = ViewToolbarBinding.bind(toolbarLayout)
            }

            toolbarDatabinding?.let {
                with(toolbarDatabinding) {
                    if (getToolbar() == null) {
                        setToolbar(toolbarModel)
                    }
                    setLifecycleOwner(lifecycleOwner)

                    with(toolbarModel) {
                        val toolbar = toolbarLayout.toolbar
                        prepareMenu(toolbar, this)
                        prepareNavigation(toolbar, this)
                        prepareSearch(toolbarDatabinding, this)
                    }

                    prepareScrollBehavior(this)

                    executePendingBindings()
                }
            }
        }
    }

    private fun prepareMenu(toolbar: Toolbar, toolbarModel: ToolbarModel) {
        with(toolbarModel) {
            // menu
            menuResId.observe(lifecycleOwner, Observer {
                it?.let {
                    toolbar.menu.clear()
                    if (it != ToolbarModel.PARAMETER_NOT_SET) {
                        toolbar.inflateMenu(it)
                    }
                }
            })
            menuItemClickListener?.let {
                toolbar.setOnMenuItemClickListener { item: MenuItem? ->
                    item?.let {
                        menuItemClickListener.invoke(item)
                    }
                    false
                }
            }
        }
    }

    private fun prepareNavigation(toolbar: Toolbar, toolbarModel: ToolbarModel) {
        with(toolbarModel) {
            // navigation
            if (hasNavigationButton) {
                toolbar.setNavigationIcon(R.drawable.ic_arrow_back)
                toolbar.setNavigationOnClickListener {
                    if (lifecycleOwner is FragmentActivity) {
                        (lifecycleOwner as FragmentActivity).onBackPressed()
                    } else if (lifecycleOwner is Fragment) {
                        (lifecycleOwner as Fragment).activity?.onBackPressed()
                    }
                }
            }
        }
    }

    private fun prepareSearch(toolbarDatabinding: ViewToolbarBinding, toolbarModel: ToolbarModel) {
        with(toolbarModel) {
            // search
            if (hasSearch) {
                val searchEditText = toolbarDatabinding.searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text) as EditText
                searchEditText.setTextColor(ContextCompat.getColor(searchEditText.context, R.color.white))
                searchEditText.setHintTextColor(ContextCompat.getColor(searchEditText.context, R.color.white))
                toolbarDatabinding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                    override fun onQueryTextSubmit(query: String?): Boolean {
                        query?.let { onQueryTextSubmit?.invoke(it) }
                        return false
                    }

                    override fun onQueryTextChange(newText: String?): Boolean {
                        newText?.let { onQueryTextChange?.invoke(it) }
                        return false
                    }
                })
            }
        }
    }

    private fun prepareScrollBehavior(toolbarDatabinding: ViewToolbarBinding) {
        // scroll-behavior
        val layoutParams = toolbarDatabinding.toolbar.layoutParams as AppBarLayout.LayoutParams
        when (toolbarModel.scrollBehavior) {
            ToolbarScrollBehavior.FIXED -> layoutParams.scrollFlags = 0
            ToolbarScrollBehavior.SCROLL_OUT ->
                layoutParams.scrollFlags = AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS or AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL
        }
    }
}