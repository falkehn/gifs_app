package dev.sieber.gifs.ui.search.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList
import dev.sieber.gifs.BuildConfig
import dev.sieber.gifs.model.Result
import dev.sieber.gifs.model.Suggestions
import dev.sieber.gifs.net.TenorRetrofitBuilder
import dev.sieber.gifs.ui.search.repository.SearchDataSourceFactory
import kotlinx.coroutines.experimental.launch


class SearchViewModel(application: Application) : AndroidViewModel(application) {

    private val tenorService by lazy { TenorRetrofitBuilder.getTenorService(application) }

    private val autocomplete = MutableLiveData<Suggestions>()
    private val suggestions = MutableLiveData<Suggestions>()
    private val trendingTerms = MutableLiveData<Suggestions>()
    private val showGifs = MutableLiveData<Boolean>()

    fun loadTrendingTerms() {
        launch {
            val response = tenorService.getTrendingTerms("DE", BuildConfig.api_key).execute()
            if (response.isSuccessful) {
                response.body()?.let {
                    trendingTerms.postValue(response.body())
                }
            }
        }
    }

    fun loadSuggestions(query: String) {
        launch {
            val response = tenorService.getSuggestions(query, "20", BuildConfig.api_key).execute()
            if (response.isSuccessful) {
                response.body()?.let {
                    suggestions.postValue(response.body())
                }
            } else {
                suggestions.postValue(Suggestions(mutableListOf()))
            }
        }
    }

    fun loadAutoComplete(query: String) {
        launch {
            val response = tenorService.getAutocomplete(query, BuildConfig.api_key).execute()
            if (response.isSuccessful) {
                response.body()?.let {
                    autocomplete.postValue(response.body())
                }
            }
        }
    }

    fun loadGifs(query: String): LiveData<PagedList<Result>> {
        val pagedListConfig = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setInitialLoadSizeHint(30)
            .setPageSize(20)
            .build()

        return LivePagedListBuilder(SearchDataSourceFactory(tenorService, query), pagedListConfig).build()
    }

    fun setQuery(query: String) = showGifs.postValue(query.length >= 2)

    fun getAutocompleteSuggestions(): LiveData<Suggestions> = autocomplete

    fun getSuggestions(): LiveData<Suggestions> = suggestions

    fun getTrendingTerms(): LiveData<Suggestions> = trendingTerms

    fun showGifs(): LiveData<Boolean> = showGifs

}