package dev.sieber.gifs.ui.toolbar

import android.arch.lifecycle.MutableLiveData
import android.support.annotation.IdRes
import android.support.annotation.MenuRes
import android.support.v4.app.FragmentActivity
import android.view.MenuItem

/**
 * A Data-Model to represent the Toolbar. Use the [ToolbarModel.Builder] class to create a [ToolbarModel] instance.
 */
data class ToolbarModel(
    @IdRes val resId: Int,
    val title: MutableLiveData<String> = MutableLiveData(),
    val hasNavigationButton: Boolean,
    val hasSearch: Boolean,
    val searchQuery: MutableLiveData<String> = MutableLiveData(),
    val onQueryTextSubmit: ((query: String) -> Boolean)?,
    val onQueryTextChange: ((newQuery: String) -> Unit)?,
    val searchViewHasFocus: MutableLiveData<Boolean> = MutableLiveData(),
    val scrollBehavior: ToolbarScrollBehavior = ToolbarScrollBehavior.SCROLL_OUT,
    val menuResId: MutableLiveData<Int> = MutableLiveData(),
    val menuItemClickListener: ((menu: MenuItem) -> Boolean)?
) {

    companion object {

        const val NO_TOOLBAR = 0
        const val PARAMETER_NOT_SET = 0

    }

    /**
     * An Utility class to create a [ToolbarModel] using the Builder Pattern.
     */
    class Builder {

        private var resId: Int = NO_TOOLBAR
        private var title: MutableLiveData<String> = MutableLiveData()
        private var hasNavigationButton = false
        private var badgeCount: MutableLiveData<Int> = MutableLiveData()
        private var scrollBehavior: ToolbarScrollBehavior = ToolbarScrollBehavior.SCROLL_OUT
        private val menuResId: MutableLiveData<Int> = MutableLiveData()
        private var menuItemClickListener: ((menu: MenuItem) -> Boolean)? = null

        // search
        private var hasSearch = false
        private var searchQuery: MutableLiveData<String> = MutableLiveData()
        private var onQueryTextSubmit: ((query: String) -> Boolean)? = null
        private var onQueryTextChange: ((newQuery: String) -> Unit)? = null
        private val searchViewHasFocus: MutableLiveData<Boolean> = MutableLiveData()

        /**
         * View-ID of the imported layout (needs to be [com.lidl.eci.R.layout.view_toolbar]
         * @param resId ID of the layout
         */
        fun withId(@IdRes resId: Int) = apply { this.resId = resId }

        /**
         * Title to display on the Toolbar. The [ToolbarModel.title] property is a [MutableLiveData] Object, so the Toolbar Title can also be
         * re-adjusted.
         *
         * @param title Title to display
         */
        fun withTitle(title: String) = apply { this.title.value = title }

        /**
         * Menu to inflate on the Toolbar. The [ToolbarModel.menuResId] property is a [MutableLiveData] Object, so the Toolbar Menu can also be
         * re-adjusted.
         *
         * @param menuId ID to inflate. Leave this field empty if no menu should be shown. To get the MenuItems translated automatically by the
         * TranslationUtils, use the translation-key as the text-value in the menu.xml
         * eg.
         * <item
         *      android:id="@+id/action_reset_filter"
         *      android:title="reset_filter"
         *      app:showAsAction="always|withText"/>
         *
         * @param menuItemClickListener function which is triggered as soon as a [MenuItem] is clicked.
         */
        fun withMenu(@MenuRes menuId: Int = 0, menuItemClickListener: ((menuItem: MenuItem) -> Boolean)?) = apply {
            this.menuResId.value = menuId
            this.menuItemClickListener = menuItemClickListener
        }

        /**
         * Display the Navigation-Back-Button. When the Back-Button is clicked, [FragmentActivity.onBackPressed] is called.
         */
        fun withDefaultNavigationButton() = apply {
            this.hasNavigationButton = true
        }

        /**
         * Executing this function ensures that the search is displayed in the Toolbar.
         * @param hint hint to display
         * @param query query to display
         * @param onQueryTextSubmit function which gets called when a search-query is submitted
         * @param onQueryTextChange function which gets called when the search-query is changed
         */
        fun withSearch(
            query: String? = null,
            onQueryTextSubmit: ((query: String) -> Boolean)? = null,
            onQueryTextChange: ((newQuery: String) -> Unit)? = null
        ) = apply {
            this.hasSearch = true
            this.searchQuery.value = query
            this.onQueryTextSubmit = onQueryTextSubmit
            this.onQueryTextChange = onQueryTextChange
        }

        /**
         * Displays a Badge with the given [badgeCount] to the right of the title. The [ToolbarModel.badgeCount] property is a [MutableLiveData] Object,
         * so the Toolbar Badge can also be re-adjusted.
         *
         * @param badgeCount count to display on the Badge
         */
        fun withBadge(badgeCount: Int) = apply {
            this.badgeCount.value = badgeCount
        }

        /**
         * There are two Toolbar-Behaviors in the App when User is scrolling the display content.
         * [ToolbarScrollBehavior.FIXED] The Toolbar stays at the top when the content moves
         * [ToolbarScrollBehavior.SCROLL_OUT] When scrolling down, the Toolbar is moved out to create more space on the display. When scrolling upwards,
         * the Toolbar is retracted again. To use this Scrolling-Behavior the Toolbar needs to have a CoordinatorLayout as it´s parent Layout.
         *
         * @param scrollBehavior scrolling behavior of the Toolbar
         */
        fun withScrollBehavior(scrollBehavior: ToolbarScrollBehavior) = apply {
            this.scrollBehavior = scrollBehavior
        }

        /**
         * Builds the [ToolbarModel]
         */
        fun build() = ToolbarModel(
            resId,
            title,
            hasNavigationButton,
            hasSearch,
            searchQuery,
            onQueryTextSubmit,
            onQueryTextChange,
            searchViewHasFocus,
            scrollBehavior,
            menuResId,
            menuItemClickListener
        )

    }
}