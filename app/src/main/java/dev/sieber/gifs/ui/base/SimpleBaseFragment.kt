package dev.sieber.gifs.ui.base

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.View
import dev.sieber.gifs.ui.toolbar.ToolbarManager
import dev.sieber.gifs.ui.toolbar.ToolbarModel

open class SimpleBaseFragment : Fragment() {

    private val toolbarModel: ToolbarModel by lazy {
        createToolbar()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ToolbarManager(this, view, toolbarModel).prepareToolbar()
    }

    protected open fun createToolbar(): ToolbarModel {
        return ToolbarModel.Builder()
            .build()
    }

}