package dev.sieber.gifs.ui.search.repository

import android.arch.paging.PageKeyedDataSource
import dev.sieber.gifs.BuildConfig
import dev.sieber.gifs.model.Result
import dev.sieber.gifs.net.TenorService

class SearchRepository(
    private val tenorService: TenorService,
    private val query: String
) : PageKeyedDataSource<String, Result>() {

    override fun loadInitial(params: LoadInitialParams<String>, callback: LoadInitialCallback<String, Result>) {
        val response = tenorService.getSearchGifs(query, params.requestedLoadSize.toString(), "0", "DE", BuildConfig.api_key).execute()
        if (response.isSuccessful) {
            response.body()?.let {
                callback.onResult(it.results, null, it.next)
            }
        }
    }

    override fun loadAfter(params: LoadParams<String>, callback: LoadCallback<String, Result>) {
        val response = tenorService.getSearchGifs(query, params.requestedLoadSize.toString(), params.key, "DE", BuildConfig.api_key).execute()
        if (response.isSuccessful) {
            response.body()?.let {
                callback.onResult(it.results, it.next)
            }
        }
    }

    override fun loadBefore(params: LoadParams<String>, callback: LoadCallback<String, Result>) = Unit

}