package dev.sieber.gifs.ui.search.view

interface SuggestionClickListener {

    fun onSuggestionClick(suggestion: String)

}