package dev.sieber.gifs.ui.preferences


import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.preference.PreferenceFragmentCompat
import android.view.View
import com.marcoscg.easylicensesdialog.EasyLicensesDialog
import dev.sieber.gifs.R
import dev.sieber.gifs.ui.toolbar.ToolbarManager
import dev.sieber.gifs.ui.toolbar.ToolbarModel

class PrefsFragment : PreferenceFragmentCompat() {

    private val toolbarModel: ToolbarModel by lazy {
        createToolbar()
    }

    override fun onCreatePreferences(p0: Bundle?, p1: String?) {
        addPreferencesFromResource(R.xml.preferences)

        val contact = findPreference(getString(R.string.pref_key_contact))
        contact.setOnPreferenceClickListener {
            val pinfo = activity?.packageManager?.getPackageInfo(activity?.packageName, 0)
            val versionName = pinfo?.versionName

            val i = Intent(Intent.ACTION_SEND)
            i.putExtra(Intent.EXTRA_EMAIL, arrayOf("dev.sieber@gmail.com"))
            i.putExtra(Intent.EXTRA_SUBJECT, "Feedback (Version: $versionName) - ${getString(R.string.app_name)}")
            i.type = "plain/text"
            activity?.startActivity(i)
            false
        }

        val rate = findPreference(getString(R.string.pref_key_rate))
        rate.setOnPreferenceClickListener {
            activity?.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=de.gifs")))
            false
        }

        val licences = findPreference(getString(R.string.pref_key_licences))
        licences.setOnPreferenceClickListener {
            val easyLicensesDialog = EasyLicensesDialog(context)
            easyLicensesDialog.setTitle(getString(R.string.pref_licences))
            easyLicensesDialog.setCancelable(true)
            easyLicensesDialog.show()
            false
        }

        val versionNumber = activity?.packageManager?.getPackageInfo(activity?.packageName, 0)?.versionName
        val version = findPreference(getString(R.string.pref_key_version))
        version.title = String.format(getString(R.string.pref_version), versionNumber)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ToolbarManager(this, view, toolbarModel).prepareToolbar()
    }

    private fun createToolbar(): ToolbarModel {
        return ToolbarModel.Builder()
            .withId(R.id.toolbarLayout)
            .withTitle(getString(R.string.menu_settings))
            .build()
    }

}