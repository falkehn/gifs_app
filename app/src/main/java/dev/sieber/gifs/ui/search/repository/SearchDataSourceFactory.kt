package dev.sieber.gifs.ui.search.repository

import android.arch.paging.DataSource
import dev.sieber.gifs.model.Result
import dev.sieber.gifs.net.TenorService

class SearchDataSourceFactory(
    private val tenorService: TenorService,
    private val query: String
) : DataSource.Factory<String, Result>() {

    override fun create(): DataSource<String, Result> = SearchRepository(tenorService, query)

}