package dev.sieber.gifs.ui.search.view

import android.support.v7.widget.RecyclerView
import dev.sieber.gifs.databinding.ItemSuggestionBinding
import org.jetbrains.anko.sdk25.coroutines.onClick

class SuggestionViewHolder(private var binding: ItemSuggestionBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(suggestion: String, suggestionClickListener: SuggestionClickListener) {
        with(binding) {
            this.name = suggestion
            executePendingBindings()

            itemView.onClick {
                suggestionClickListener.onSuggestionClick(suggestion)
            }
        }
    }

}