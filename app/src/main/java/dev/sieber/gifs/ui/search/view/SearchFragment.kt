package dev.sieber.gifs.ui.search.view

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.StaggeredGridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hannesdorfmann.fragmentargs.annotation.Arg
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs
import dev.sieber.gifs.R
import dev.sieber.gifs.databinding.FragmentSearchBinding
import dev.sieber.gifs.model.Result
import dev.sieber.gifs.ui.base.SimpleBaseFragment
import dev.sieber.gifs.ui.search.viewmodel.SearchViewModel
import dev.sieber.gifs.ui.toolbar.ToolbarModel
import kotlinx.android.synthetic.main.fragment_search.autocompleteList
import kotlinx.android.synthetic.main.fragment_search.searchList
import kotlinx.android.synthetic.main.fragment_search.suggestionList
import kotlinx.android.synthetic.main.fragment_search.trendingTermList
import timber.log.Timber


@FragmentWithArgs
class SearchFragment : SimpleBaseFragment(), SuggestionClickListener, SearchClickListener {

    companion object {

        const val SEARCH_KEY = "SEARCH_KEY"

    }

    @Arg
    var searchQuery: String = ""

    private val searchAdapter = SearchAdapter(this)

    private val vmSearch by lazy {
        ViewModelProviders.of(this).get(SearchViewModel::class.java)
    }

    private var rootView: View? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.fragment_search)
//
//        val searchWord = ObjectCache.getInstance().getObject(SEARCH_KEY) as String?

//        title = searchWord
//        supportActionBar.setDisplayHomeAsUpEnabled(true)

        // log event
//        val firebaseAnalytics = FirebaseAnalytics.getInstance(this)
//        val bundle = Bundle()
//        bundle.putString(FirebaseAnalytics.Param.SEARCH_TERM, searchWord)
//        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SEARCH, bundle)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView?.let {
            return rootView
        }

        val dataBinding = FragmentSearchBinding.inflate(inflater, container, false)
        dataBinding.vmSearch = vmSearch
        dataBinding.setLifecycleOwner(this)
        rootView = dataBinding.root
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // TrendingTerm
        val trendingTermsAdapter = SuggestionAdapter(this)
        trendingTermList.adapter = trendingTermsAdapter
        trendingTermList.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        vmSearch.getTrendingTerms().observe(this, Observer { trendingTerms ->
            trendingTerms?.let {
                trendingTermsAdapter.setSuggestions(trendingTerms.results)
            }
        })
        vmSearch.loadTrendingTerms()

        // Autocomplete
        val autocompleteAdapter = SuggestionAdapter(this)
        autocompleteList.adapter = autocompleteAdapter
        autocompleteList.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        vmSearch.getAutocompleteSuggestions().observe(this, Observer { autocomplete ->
            autocomplete?.let {
                autocompleteAdapter.setSuggestions(autocomplete.results)
            }
        })
        autocompleteList.isNestedScrollingEnabled = false

        // Suggestions
        val suggestionsAdapter = SuggestionAdapter(this)
        suggestionList.adapter = suggestionsAdapter
        suggestionList.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        vmSearch.getSuggestions().observe(this, Observer { suggestions ->
            suggestions?.let {
                suggestionsAdapter.setSuggestions(suggestions.results)
            }
        })
        suggestionList.isNestedScrollingEnabled = false

        // Search-Results
        val staggeredGridLayoutManager = StaggeredGridLayoutManager(2, 1)
        searchList.layoutManager = staggeredGridLayoutManager
        searchList.adapter = searchAdapter
    }

    override fun createToolbar(): ToolbarModel {
        return ToolbarModel.Builder()
            .withId(R.id.toolbarLayout)
            .withSearch(query = "Search",
                onQueryTextSubmit = { query: String ->
                    Timber.i("Query: $query")
                    false
                },
                onQueryTextChange = { newQuery: String ->
                    vmSearch.setQuery(newQuery)
                    if (vmSearch.showGifs().value == true) {
                        vmSearch.loadAutoComplete(newQuery)
                        vmSearch.loadSuggestions(newQuery)
                        vmSearch.loadGifs(newQuery).observe(this, Observer { gifs ->
                            gifs?.run {
                                searchAdapter.submitList(gifs)
                            }
                        })
                    }
                })
            .withDefaultNavigationButton()
            .build()
    }

    override fun onSuggestionClick(suggestion: String) {
    }

    override fun onGifClickListener(result: Result) {
    }
}
