package dev.sieber.gifs.ui.search.view

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import dev.sieber.gifs.R

class SuggestionAdapter(private val suggestionClickListener: SuggestionClickListener) : RecyclerView.Adapter<SuggestionViewHolder>() {

    private val suggestions: MutableList<String> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SuggestionViewHolder {
        return SuggestionViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_suggestion,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: SuggestionViewHolder, position: Int) {
        holder.bind(suggestions[position], suggestionClickListener)
    }

    override fun getItemCount() = suggestions.size

    fun setSuggestions(suggestions: List<String>) {
        this.suggestions.clear()
        this.suggestions.addAll(suggestions)
        notifyDataSetChanged()
    }
}