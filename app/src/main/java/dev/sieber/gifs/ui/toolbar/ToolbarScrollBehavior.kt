package dev.sieber.gifs.ui.toolbar


/**
 * Defines what should happen with the [android.widget.Toolbar] when the user start´s scrolling.
 */
enum class ToolbarScrollBehavior {
    /**
     * Stay fixed at the top
     */
    FIXED,
    /**
     * Animate out of the Screen when User is scrolling.
     */
    SCROLL_OUT
}