package dev.sieber.gifs.ui.search.view

import dev.sieber.gifs.model.Result

interface SearchClickListener {

    fun onGifClickListener(result: Result)

}