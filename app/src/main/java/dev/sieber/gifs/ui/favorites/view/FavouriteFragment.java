package dev.sieber.gifs.ui.favorites.view;

import dev.sieber.gifs.R;
import dev.sieber.gifs.adapter.GifAdapter;
import dev.sieber.gifs.fragments.BaseListFragment;
import dev.sieber.gifs.model.GifImages;
import dev.sieber.gifs.utils.FavouriteUtils;
import dev.sieber.gifs.utils.GifUtils;

public class FavouriteFragment extends BaseListFragment implements GifAdapter.OnClickListener {

    public static FavouriteFragment newInstance() {
        return new FavouriteFragment();
    }

    @Override
    public void onResume() {
        super.onResume();
        requestGifs("");
    }

    @Override
    protected void requestGifs(final String offset) {
        super.requestGifs(offset);
        //read favourite gifs from file-system
        final GifImages gifImages = FavouriteUtils.getFavouriteGifImages();
        if (gifImages == null || gifImages.getResults().size() == 0) {
            handleViewVisibility(ViewType.Empty);
            GifUtils.animateEmptyScreen(mEmptyView, getString(R.string.no_favourites_title), getString(R.string.no_favourites_description));
        } else {
            handleViewVisibility(ViewType.Data);
            mGifImages = gifImages;
            mGifAdapter.setData(mGifImages.getResults());
        }
    }

    @Override
    public String getTitle() {
        return getString(R.string.menu_favourites);
    }
}
