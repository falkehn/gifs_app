package dev.sieber.gifs.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class PurchaseUtils {

    public static void setPurchased(final Context context, final String purchaseId, final boolean isPurchased) {
        SharedPreferences sharedPreferences = getSharedPreferences(context);
        sharedPreferences.edit().putBoolean(purchaseId, isPurchased).commit();
    }

    public static boolean isPurchased(final Context context, final String purchaseId) {
        SharedPreferences sharedPreferences = getSharedPreferences(context);
        return sharedPreferences.getBoolean(purchaseId, false);
    }

    private static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences("de.gifs", Context.MODE_PRIVATE);
    }
}
