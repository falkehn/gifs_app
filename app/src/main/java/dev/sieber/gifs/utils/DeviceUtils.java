package dev.sieber.gifs.utils;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.view.Display;
import android.view.WindowManager;

import java.util.Locale;

public class DeviceUtils {

    private static int sDeviceWidth;

    public static int getDeviceWidth(final Context context) {
        if (sDeviceWidth == 0) {
            final WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
            final Display display = wm.getDefaultDisplay();
            final Point size = new Point();
            display.getSize(size);
            sDeviceWidth = size.x;
        }
        return sDeviceWidth;
    }

    public static String getLanguageCode() {
        final String country = Locale.getDefault().getCountry();
        final String lang = Locale.getDefault().getISO3Language().substring(0, 2);
        return lang + "_" + country;
    }

    public static boolean isAppInstalled(final Context context, final String uri) {
        final PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return false;
    }
}
