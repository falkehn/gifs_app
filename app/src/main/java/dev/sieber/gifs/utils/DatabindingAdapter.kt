package dev.sieber.gifs.utils

import android.databinding.BindingAdapter
import android.widget.ImageView

object DatabindingAdapter {

    @JvmStatic
    @BindingAdapter("load--gif")
    fun loadImage(imageView: ImageView, imageUrl: String) {
        GifUtils.loadGif(imageUrl, imageView)
    }
}