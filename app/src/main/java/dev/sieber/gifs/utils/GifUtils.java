package dev.sieber.gifs.utils;

import android.graphics.Typeface;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import dev.sieber.gifs.R;

public class GifUtils {

    public static void loadGif(final String url, final ImageView into, final FutureCallback<ImageView> callback) {
        Ion
                .with(into.getContext())
                .load(url)
                .intoImageView(into)
                .setCallback(callback);
    }

    public static void loadGif(final String url, final ImageView into) {
        loadGif(url, into, null);
    }

    public static void animateProgress(final ViewGroup parentLayout) {
        final ImageView imageView = parentLayout.findViewById(R.id.progress_image);
        Ion
                .with(imageView.getContext())
                .load("android.resource://" + imageView.getContext().getPackageName() + "/drawable/loading")
                .intoImageView(imageView);

        final TextView textView = (TextView) parentLayout.findViewById(R.id.loading);
        if (textView != null) {
            final Typeface typeface = Typeface.createFromAsset(parentLayout.getContext().getAssets(), "nevis.ttf");
            textView.setTypeface(typeface);
        }
    }

    public static void animateEmptyScreen(final ViewGroup parentLayout, final String title, final String description) {
        final Typeface typeface = Typeface.createFromAsset(parentLayout.getContext().getAssets(), "nevis.ttf");

        final TextView titleTv = parentLayout.findViewById(R.id.empty_text_title);
        titleTv.setText(title);
        titleTv.setTypeface(typeface);

        final TextView descriptionTv = parentLayout.findViewById(R.id.empty_text_description);
        descriptionTv.setText(description);
        descriptionTv.setTypeface(typeface);
    }
}
