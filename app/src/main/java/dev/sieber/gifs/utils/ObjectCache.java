package dev.sieber.gifs.utils;

import java.util.HashMap;

public class ObjectCache {

    private static ObjectCache sInstance;

    private HashMap<String, Object> mCache;

    public static ObjectCache getInstance() {
        if (sInstance == null) {
            sInstance = new ObjectCache();
        }
        return sInstance;
    }

    private ObjectCache() {
        mCache = new HashMap<>();
    }

    public void putObject(final String key, final Object object) {
        mCache.put(key, object);
    }

    public Object getObject(final String key) {
        return mCache.get(key);
    }

    public void clearCache() {
        mCache.clear();
    }
}
