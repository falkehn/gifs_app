package dev.sieber.gifs.utils;

import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import dev.sieber.gifs.model.GifImages;
import dev.sieber.gifs.model.Image;
import dev.sieber.gifs.model.Images;
import dev.sieber.gifs.model.Result;

public class FavouriteUtils {

    public static boolean isFavourite(final String id) {
        final List<File> files = getFavorites();
        for (final File file : files) {
            if (getIdFromFileName(file).equals(id)) {
                return true;
            }
        }
        return false;
    }

    private static String getIdFromFileName(final File file) {
        final int indexOf = file.getName().indexOf(".gif");
        return file.getName().substring(0, indexOf).split("_")[3];
    }

    public static GifImages getFavouriteGifImages() {
        final List<Result> dataList = new ArrayList<>();
        final List<File> files = getFavorites();
        for (final File file : files) {
            final Result data = new Result();
            data.setId(getIdFromFileName(file));
            final Image image = new Image();
            image.setUrl(file.getAbsolutePath());

            final int indexOf = file.getName().indexOf("gif");
            final String widthAndHeight = file.getName().substring(0, indexOf);
            image.setDims(new int[2]);
            image.getDims()[0] = Integer.valueOf(widthAndHeight.split("_")[0]);
            image.getDims()[1] = Integer.valueOf(widthAndHeight.split("_")[1]);

            final List<Images> imagesList = new ArrayList<>();
            final Images images = new Images();
            images.setGif(image);
            images.setTinygif(image);
            imagesList.add(images);
            data.setMedia(imagesList);
            dataList.add(data);
        }
        final GifImages gifImages = new GifImages(dataList, "");
        return gifImages;
    }

    private static List<File> getFavorites() {
        final List<File> retVal = new ArrayList<>();

        final String root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString();
        final File gifsRoot = new File(root + "/gifs");
        if (!gifsRoot.exists()) {
            gifsRoot.mkdir();
        }

        final File[] bitmapFiles = gifsRoot.listFiles();
        if (bitmapFiles != null) {
            for (File f : bitmapFiles) {
                retVal.add(f);
            }
        }

        return retVal;
    }

    public static void save(final String id, final Image image) {
        final String root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString();
        final File gifsRoot = new File(root + "/gifs");
        if (!gifsRoot.exists()) {
            gifsRoot.mkdir();
        }

        final String fileName = image.getDims()[0] + "_" + image.getDims()[1] + "_" + "gif_" + id + ".gif";
        final File mypath = new File(gifsRoot, fileName);
        downloadFile(image.getUrl(), mypath);
    }

    public static void save(final Image image, final String fileName) {
        final String root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString();
        final File gifsRoot = new File(root + "/gifs");
        if (!gifsRoot.exists()) {
            gifsRoot.mkdir();
        }

        final File mypath = new File(gifsRoot, fileName);
        downloadFile(image.getUrl(), mypath);
    }

    public static File getFile(final String id) {
        final List<File> files = getFavorites();
        for (final File file : files) {
            if (getIdFromFileName(file).equals(id)) {
                return file;
            }
        }
        return null;
    }

    public static void delete(final String id) {
        final List<File> files = getFavorites();
        for (final File file : files) {
            if (getIdFromFileName(file).equals(id)) {
                file.delete();
            }
        }
    }

    private static boolean downloadFile(String urlStr, File file) {
        try {
            final URL url = new URL(urlStr);
            final HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.connect();
            final FileOutputStream fileOutput = new FileOutputStream(file);
            final InputStream inputStream = urlConnection.getInputStream();

            final byte[] buffer = new byte[1024];
            int bufferLength; //used to store a temporary size of the buffer

            //now, read through the input buffer and write the contents to the file
            while ((bufferLength = inputStream.read(buffer)) > 0) {
                //add the results in the buffer to the file in the file output stream (the file on the sd card
                fileOutput.write(buffer, 0, bufferLength);
            }
            //close the output stream when done
            fileOutput.close();
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
