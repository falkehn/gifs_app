package dev.sieber.gifs.utils;

import android.content.Context;
import android.view.View;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

import dev.sieber.gifs.R;

public class AdUtils {

    private static AdUtils sInstance;
    private AdView mAdBanner;
    private InterstitialAd mAdInterstitial;

    public static AdUtils getInstance() {
        if (sInstance == null) {
            sInstance = new AdUtils();
        }
        return sInstance;
    }

    public void showAdBanner(final AdView adView) {
        if (PurchaseUtils.isPurchased(adView.getContext(), adView.getContext().getString(R.string.purchase_remove_ads))) {
            return;
        }
        mAdBanner = adView;
        mAdBanner.loadAd(getAdRequest());
        mAdBanner.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
            }

            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
            }

            @Override
            public void onAdLeftApplication() {
                super.onAdLeftApplication();
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                mAdBanner.setVisibility(View.VISIBLE);
            }
        });
    }

    public void loadFullscreenAd(final Context context) {
        if (PurchaseUtils.isPurchased(context, context.getString(R.string.purchase_remove_ads))) {
            return;
        }

        mAdInterstitial = new InterstitialAd(context);
        mAdInterstitial.setAdUnitId(context.getString(R.string.ad_interstitial));
        mAdInterstitial.loadAd(getAdRequest());
        mAdInterstitial.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
            }

            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
            }

            @Override
            public void onAdLeftApplication() {
                super.onAdLeftApplication();
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                mAdBanner.setVisibility(View.VISIBLE);
            }
        });
    }

    public void displayFullScreenAd(final Context context) {
        if (PurchaseUtils.isPurchased(context, context.getString(R.string.purchase_remove_ads))) {
            return;
        }

        mAdInterstitial.show();
    }

    private AdRequest getAdRequest() {
        return new AdRequest.Builder()
                .addTestDevice("CBC0D09CA13A28E8E89AAB936EBB5650")
                .addTestDevice("AD42C859A02F24A55C92064B2697449E")
                .addTestDevice("53FD2FCEE297FFF37BCC761C56F4B05C")
                .addTestDevice("25889F3F9973F721B42821D62EEADF44")
                .build();
    }

    public void hideAds() {
        if (mAdBanner != null) {
            mAdBanner.setVisibility(View.GONE);
        }
    }
}
