package dev.sieber.gifs.utils;

import android.app.Activity;
import android.content.Intent;

public class UIUtils {

    public static void openEmailIntent(Activity activity, String subject) {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.putExtra(Intent.EXTRA_EMAIL, new String[]{"tomasz.sieber@gmx.de"});
        i.putExtra(Intent.EXTRA_SUBJECT, subject);
        i.setType("plain/text");
        activity.startActivity(i);
    }
}
