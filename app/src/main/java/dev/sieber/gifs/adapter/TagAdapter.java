package dev.sieber.gifs.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import dev.sieber.gifs.R;
import dev.sieber.gifs.model.Tag;
import dev.sieber.gifs.utils.DeviceUtils;
import dev.sieber.gifs.utils.GifUtils;

public class TagAdapter extends RecyclerView.Adapter<TagAdapter.TagViewHolder> {

    private List<Tag> mData;
    private OnClickListener mOnClickListener;

    public TagAdapter(final List<Tag> tags) {
        mData = tags;
    }

    @Override
    public TagViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_tag, parent, false);
        return new TagViewHolder(v);
    }

    @Override
    public void onBindViewHolder(TagViewHolder holder, final int position) {
        final Tag data = mData.get(position);
        final int imageWidth = DeviceUtils.getDeviceWidth(holder.imageView.getContext()) / 2;
        holder.imageView.setBackgroundColor(holder.imageView.getResources().getColor(getRandomBackgroundColor()));
        holder.imageView.setLayoutParams(new RelativeLayout.LayoutParams(imageWidth, imageWidth));
        holder.imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        holder.imageView.setOnClickListener(view -> mOnClickListener.onClickImage(position));
        holder.title.setText(data.getName().toUpperCase());
        GifUtils.loadGif(data.getImage(), holder.imageView);
    }

    @Override
    public int getItemCount() {
        if (mData != null) {
            return mData.size();
        }
        return 0;
    }

    public void setOnClickListener(final OnClickListener onClickListener) {
        mOnClickListener = onClickListener;
    }

    private int getRandomBackgroundColor() {
        final int[] backgroundColors = {
                R.color.item_backgground_1,
                R.color.item_backgground_2,
                R.color.item_backgground_3,
                R.color.item_backgground_4,
                R.color.item_backgground_5,
                R.color.item_backgground_6,
                R.color.item_backgground_7,
                R.color.item_backgground_8,
        };
        final Random random = new Random();
        final int position = random.nextInt(backgroundColors.length - 1 - 0 + 1) + 0;
        return backgroundColors[position];
    }

    public interface OnClickListener {
        void onClickImage(final int position);
    }

    public class TagViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imageview)
        ImageView imageView;

        @BindView(R.id.title)
        TextView title;

        TagViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
