package dev.sieber.gifs.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import dev.sieber.gifs.R;
import dev.sieber.gifs.model.Image;
import dev.sieber.gifs.model.Result;
import dev.sieber.gifs.utils.DeviceUtils;
import dev.sieber.gifs.utils.GifUtils;

public class GifAdapter extends RecyclerView.Adapter<GifAdapter.GifViewHolder> {

    private List<Result> mData;
    private OnClickListener mOnClickListener;

    @Override
    public GifViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_imageview, parent, false);
        return new GifViewHolder(v);
    }

    @Override
    public void onBindViewHolder(GifViewHolder holder, final int position) {
        final Result data = mData.get(position);
        final Image image = data.getMedia().get(0).getTinygif();
        final int imageWidth = DeviceUtils.getDeviceWidth(holder.imageView.getContext()) / 2;

        //calc height
        final Image orginal = data.getMedia().get(0).getGif();
        final int height = orginal.getDims()[1] * imageWidth / orginal.getDims()[0];

        holder.imageView.setLayoutParams(new ViewGroup.LayoutParams(imageWidth, height));

        holder.imageView.setBackgroundColor(holder.imageView.getResources().getColor(getRandomBackgroundColor()));

        GifUtils.loadGif(image.getUrl(), holder.imageView);

        holder.imageView.setOnClickListener(view -> mOnClickListener.onClickImage(position));
    }

    @Override
    public int getItemCount() {
        if (mData != null) {
            return mData.size();
        }
        return 0;
    }

    public Result getItem(final int position) {
        return mData.get(position);
    }

    public void setData(final List<Result> data) {
        mData = data;
        notifyDataSetChanged();
    }

    public void setOnClickListener(final OnClickListener onClickListener) {
        mOnClickListener = onClickListener;
    }

    private int getRandomBackgroundColor() {
        final int[] backgroundColors = {
                R.color.item_backgground_1,
                R.color.item_backgground_2,
                R.color.item_backgground_3,
                R.color.item_backgground_4,
                R.color.item_backgground_5,
                R.color.item_backgground_6,
                R.color.item_backgground_7,
                R.color.item_backgground_8,
        };
        final Random random = new Random();
        final int position = random.nextInt(backgroundColors.length - 1 - 0 + 1) + 0;
        return backgroundColors[position];
    }

    public interface OnClickListener {
        void onClickImage(final int position);
    }

    public class GifViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imageview)
        ImageView imageView;

        GifViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
