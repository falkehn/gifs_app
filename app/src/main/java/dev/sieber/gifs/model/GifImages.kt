package dev.sieber.gifs.model

data class GifImages(
    var results: List<Result>,
    var next: String
)