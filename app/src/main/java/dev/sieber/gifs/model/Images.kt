package dev.sieber.gifs.model

data class Images(
    var tinygif: Image? = null,
    var gif: Image? = null
)