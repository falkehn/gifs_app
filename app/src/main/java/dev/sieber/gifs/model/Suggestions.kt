package dev.sieber.gifs.model

data class Suggestions(
    val results: List<String>
)