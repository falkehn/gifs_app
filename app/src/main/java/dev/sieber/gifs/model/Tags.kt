package dev.sieber.gifs.model

data class Tags(
    var tags: List<Tag>? = null
)