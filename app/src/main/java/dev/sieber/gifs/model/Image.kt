package dev.sieber.gifs.model

import java.util.Arrays

data class Image(
    var url: String? = null,
    var preview: String? = null,
    var dims: IntArray? = null
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Image

        if (url != other.url) return false
        if (preview != other.preview) return false
        if (!Arrays.equals(dims, other.dims)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = url?.hashCode() ?: 0
        result = 31 * result + (preview?.hashCode() ?: 0)
        result = 31 * result + (dims?.let { Arrays.hashCode(it) } ?: 0)
        return result
    }
}