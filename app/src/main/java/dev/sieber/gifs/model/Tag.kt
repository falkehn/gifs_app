package dev.sieber.gifs.model

data class Tag (
    var searchterm: String? = null,
    var path: String? = null,
    var image: String? = null,
    var name: String? = null
)