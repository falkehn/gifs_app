package dev.sieber.gifs.model

data class Result(
    var id: String? = null,
    var url: String? = null,
    var media: List<Images>? = null,
    var tags: List<String>? = null,
    var title: String? = null
)