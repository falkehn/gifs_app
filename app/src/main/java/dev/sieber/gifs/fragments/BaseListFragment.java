package dev.sieber.gifs.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.mugen.Mugen;
import com.mugen.MugenCallbacks;
import com.mugen.attachers.BaseAttacher;

import org.jetbrains.annotations.NotNull;

import butterknife.BindView;
import butterknife.ButterKnife;
import dev.sieber.gifs.R;
import dev.sieber.gifs.activities.GifDetailActivity;
import dev.sieber.gifs.activities.MainActivity;
import dev.sieber.gifs.adapter.GifAdapter;
import dev.sieber.gifs.model.GifImages;
import dev.sieber.gifs.ui.base.SimpleBaseFragment;
import dev.sieber.gifs.ui.search.view.SearchFragment;
import dev.sieber.gifs.ui.search.view.SearchFragmentBuilder;
import dev.sieber.gifs.ui.toolbar.ToolbarModel;
import dev.sieber.gifs.utils.ObjectCache;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by adamczykt on 02.12.16.
 */

public abstract class BaseListFragment extends SimpleBaseFragment implements GifAdapter.OnClickListener, Callback<GifImages> {

    protected GifImages mGifImages;
    protected GifAdapter mGifAdapter;
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.layout_empty)
    protected RelativeLayout mEmptyView;
    @BindView(R.id.layout_progess)
    RelativeLayout mProgressView;
    private boolean mIsLoading;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View rootContainer = inflater.inflate(R.layout.fragment_list, container, false);
        ButterKnife.bind(this, rootContainer);

        initUI();
        if (mGifImages == null) {
            handleViewVisibility(ViewType.Progress);
            requestGifs("");
        } else {
            mGifAdapter.setData(mGifImages.getResults());
            handleViewVisibility(mGifImages.getResults().size() > 0 ? ViewType.Data : ViewType.Empty);
        }

        return rootContainer;
    }

    private void initUI() {
        mRecyclerView.setHasFixedSize(true);

        final StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, 1);
        mRecyclerView.setLayoutManager(staggeredGridLayoutManager);

        mGifAdapter = new GifAdapter();
        mGifAdapter.setOnClickListener(this);
        mRecyclerView.setAdapter(mGifAdapter);

        final BaseAttacher baseAttacher = Mugen.with(mRecyclerView, new MugenCallbacks() {
            @Override
            public void onLoadMore() {
                Log.i("TAG", "onLoadMore");
                requestGifs(mGifImages.getNext());
            }

            @Override
            public boolean isLoading() {
                return mIsLoading;
            }

            @Override
            public boolean hasLoadedAllItems() {
                return false;
            }
        });
        baseAttacher.setLoadMoreOffset(10);
        baseAttacher.start();

//        GifUtils.animateEmptyScreen(mEmptyView, null, null);
    }

    protected void handleViewVisibility(final ViewType viewType) {
        if (viewType == ViewType.Data) {
            mEmptyView.setVisibility(View.GONE);
            mProgressView.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
        } else if (viewType == ViewType.Empty) {
            mEmptyView.setVisibility(View.VISIBLE);
            mProgressView.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.GONE);
        } else if (viewType == ViewType.Progress) {
            mEmptyView.setVisibility(View.GONE);
            mProgressView.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClickImage(int position) {
        ObjectCache.getInstance().putObject(GifDetailActivity.IMAGE_DATA, mGifImages.getResults().get(position));
        final Intent intent = new Intent(getContext(), GifDetailActivity.class);
        startActivity(intent);
    }

    protected void requestGifs(String next) {
        mIsLoading = true;
    }

    //region Network-Communication
    @Override
    public void onResponse(Call<GifImages> call, Response<GifImages> response) {
        mIsLoading = false;
        if (mGifImages != null) {
            mGifImages.getResults().addAll(response.body().getResults());
            mGifImages.setNext(response.body().getNext());
        } else {
            mGifImages = response.body();
        }
        final Handler handler = new Handler();
        handler.postDelayed(() -> handleViewVisibility(mGifImages.getResults().size() > 0 ? ViewType.Data : ViewType.Empty), 1000);
        mGifAdapter.setData(mGifImages.getResults());
    }

    @Override
    public void onFailure(Call<GifImages> call, Throwable t) {
        mIsLoading = false;
    }
    //endregion Network-Communication

    @NotNull
    @Override
    protected ToolbarModel createToolbar() {
        return new ToolbarModel.Builder()
                .withId(R.id.toolbarLayout)
                .withTitle(getTitle())
                .withMenu(R.menu.menu_search, menuItem -> {
                    if (menuItem.getItemId() == R.id.action_search) {
                        final SearchFragment searchFragment = SearchFragmentBuilder.newSearchFragment("");
                        ((MainActivity) getActivity()).addFragmentOnTop(searchFragment);

                    }
                    return false;
                })
                .build();
    }

    protected String getTitle() {
        return "";
    }

    protected enum ViewType {
        Data,
        Progress,
        Empty,
        Error,
        NoInternet
    }
}
