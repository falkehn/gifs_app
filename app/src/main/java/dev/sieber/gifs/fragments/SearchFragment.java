package dev.sieber.gifs.fragments;

import dev.sieber.gifs.BuildConfig;
import dev.sieber.gifs.model.GifImages;
import dev.sieber.gifs.net.TenorRetrofitBuilder;
import dev.sieber.gifs.utils.DeviceUtils;
import retrofit2.Call;

public class SearchFragment extends BaseListFragment {

    private String mSearchWord;

    public static SearchFragment newInstance() {
        return new SearchFragment();
    }

    @Override
    protected void requestGifs(String offset) {
        requestSearchGifs(offset);
    }

    public void setSearchWord(final String searchWord) {
        mSearchWord = searchWord;
        mGifImages = null;
        if (mEmptyView != null) {
            handleViewVisibility(ViewType.Progress);
        }
        requestSearchGifs("");
    }

    private void requestSearchGifs(final String offset) {
        super.requestGifs(offset);
        final Call<GifImages> searchGifs = TenorRetrofitBuilder.getTenorService(getContext()).getSearchGifs(mSearchWord,
                "30", String.valueOf(offset), DeviceUtils.getLanguageCode(), BuildConfig.api_key);
        searchGifs.enqueue(this);
    }
}
