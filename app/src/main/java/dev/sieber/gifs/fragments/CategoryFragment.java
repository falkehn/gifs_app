package dev.sieber.gifs.fragments;

import android.os.Handler;
import android.support.v7.widget.GridLayoutManager;

import dev.sieber.gifs.BuildConfig;
import dev.sieber.gifs.R;
import dev.sieber.gifs.adapter.TagAdapter;
import dev.sieber.gifs.model.Tags;
import dev.sieber.gifs.net.TenorRetrofitBuilder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoryFragment extends BaseListFragment {

    public static CategoryFragment newInstance() {
        return new CategoryFragment();
    }

    @Override
    public void onResume() {
        super.onResume();

        final GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 2);
        mRecyclerView.setLayoutManager(layoutManager);
        final Call<Tags> searchGifs = TenorRetrofitBuilder.getTenorService(getContext()).getTags(BuildConfig.api_key);
        searchGifs.enqueue(new Callback<Tags>() {
            @Override
            public void onResponse(Call<Tags> call, Response<Tags> response) {
                final Tags tags = response.body();
                final Handler handler = new Handler();
                handler.postDelayed(() -> handleViewVisibility(tags.getTags().size() > 0 ? ViewType.Data : ViewType.Empty), 1000);
                final TagAdapter tagAdapter = new TagAdapter(tags.getTags());
                tagAdapter.setOnClickListener(position -> {
//                    ObjectCache.getInstance().putObject(SearchActivity.SEARCH_KEY, tags.tags.get(position).searchterm);
//                    final Intent intent = new Intent(getContext(), SearchActivity.class);
//                    startActivity(intent);
                });
                mRecyclerView.setAdapter(tagAdapter);
            }

            @Override
            public void onFailure(Call<Tags> call, Throwable t) {
                handleViewVisibility(ViewType.Empty);
            }
        });
    }

    @Override
    protected void requestGifs(String offset) {
        super.requestGifs(offset);
    }

    @Override
    protected String getTitle() {
        return getString(R.string.menu_categories);
    }
}
