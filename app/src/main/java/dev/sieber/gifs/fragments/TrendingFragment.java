package dev.sieber.gifs.fragments;

import dev.sieber.gifs.BuildConfig;
import dev.sieber.gifs.R;
import dev.sieber.gifs.model.GifImages;
import dev.sieber.gifs.net.TenorRetrofitBuilder;
import retrofit2.Call;

public class TrendingFragment extends BaseListFragment {

    public static TrendingFragment newInstance() {
        return new TrendingFragment();
    }

    @Override
    protected void requestGifs(final String offset) {
        super.requestGifs(offset);
//        if (mGifImages != null) {
//            handleViewVisibility(mGifImages.getResults().size() > 0 ? ViewType.Data : ViewType.Empty);
//            mGifAdapter.setData(mGifImages.getResults());
//            return;
//        }
        final Call<GifImages> trendingImagesCall = TenorRetrofitBuilder.getTenorService(getContext()).getTrendingGifs("50", BuildConfig.api_key);
        trendingImagesCall.enqueue(this);
    }

    @Override
    protected String getTitle() {
        return getString(R.string.menu_trending);
    }

}
