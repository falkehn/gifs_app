package dev.sieber.gifs.net;

import dev.sieber.gifs.model.GifImages;
import dev.sieber.gifs.model.Suggestions;
import dev.sieber.gifs.model.Tags;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface TenorService {

    String BACKEND = "https://api.tenor.com/";

    @GET("v1/search")
    Call<GifImages> getSearchGifs(
            @Query("q") String query,
            @Query("limit") String limit,
            @Query("pos") String offset,
            @Query("locale") String language,
            @Query("key") String apiKey
    );

    @GET("v1/trending")
    Call<GifImages> getTrendingGifs(
            @Query("limit") String limit,
            @Query("key") String apiKey
    );

    @GET("v1/tags")
    Call<Tags> getTags(
            @Query("key") String apiKey
    );

    @GET("v1/search_suggestions")
    Call<Suggestions> getSuggestions(
            @Query("q") String query,
            @Query("limit") String limit,
            @Query("key") String apiKey
    );

    @GET("v1/autocomplete")
    Call<Suggestions> getAutocomplete(
            @Query("q") String query,
            @Query("key") String apiKey
    );

    @GET("v1/trending_terms")
    Call<Suggestions> getTrendingTerms(
            @Query("locale") String language,
            @Query("key") String apiKey
    );
}
