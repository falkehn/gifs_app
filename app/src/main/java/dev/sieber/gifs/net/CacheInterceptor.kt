package dev.sieber.gifs.net

import android.content.Context
import android.net.ConnectivityManager
import okhttp3.Interceptor
import okhttp3.Response

class CacheInterceptor(private val context: Context) : Interceptor {

    override fun intercept(chain: Interceptor.Chain?): Response? {
        val originalResponse = chain?.proceed(chain.request())
        val maxStale = 60 * 60 * 4 // tolerate 4 hours stale
        return if (isOnline(context)) {
            originalResponse?.newBuilder()
                ?.header("Cache-Control", "public, max-age=" + maxStale)
                ?.build()

        } else {
            originalResponse?.newBuilder()
                ?.header("Cache-Control", "public, only-if-cached, max-stale=" + maxStale)
                ?.build()
        }
    }

    private fun isOnline(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val netInfo = cm.activeNetworkInfo
        if (netInfo != null && netInfo.isConnectedOrConnecting) {
            return true
        }
        return false
    }
}
