package dev.sieber.gifs.net;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class TenorRetrofitBuilder {

    public static TenorService getTenorService(final Context context) {
        final Gson gson = new GsonBuilder()
                .disableHtmlEscaping()
                .create();

        final File httpCacheDirectory = new File(context.getCacheDir(), "responses");
        final Long cacheSize = Long.valueOf(10 * 1024 * 1024);
        final Cache cache = new Cache(httpCacheDirectory, cacheSize);

        final HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);
        final OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .cache(cache)
                .addNetworkInterceptor(new CacheInterceptor(context))
                .build();

        final Retrofit retroFit = new Retrofit.Builder()
                .baseUrl(TenorService.BACKEND)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();

        return retroFit.create(TenorService.class);
    }
}
