package dev.sieber.gifs.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;

import dev.sieber.gifs.R;
import dev.sieber.gifs.utils.AdUtils;
import dev.sieber.gifs.utils.PurchaseUtils;

/**
 * Created by sieberto on 11.05.17.
 */

public class PurchaseActivity extends AppCompatActivity implements BillingProcessor.IBillingHandler {

    protected BillingProcessor mBillingProcessor;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBillingProcessor = new BillingProcessor(this, getString(R.string.playstore_licence_key), this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!mBillingProcessor.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!PurchaseUtils.isPurchased(this, getString(R.string.purchase_remove_ads))) {
            getMenuInflater().inflate(R.menu.ads, menu);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_remove_ads:
                mBillingProcessor.purchase(this, getString(R.string.purchase_remove_ads));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onProductPurchased(String productId, TransactionDetails details) {
        PurchaseUtils.setPurchased(this, productId, true);
        supportInvalidateOptionsMenu();
        AdUtils.getInstance().hideAds();
    }

    @Override
    public void onPurchaseHistoryRestored() {
    }

    @Override
    public void onBillingError(int errorCode, Throwable error) {
    }

    @Override
    public void onBillingInitialized() {
    }
}