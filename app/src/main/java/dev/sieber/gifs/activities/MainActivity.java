package dev.sieber.gifs.activities;

import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;

import com.google.android.gms.ads.MobileAds;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import dev.sieber.gifs.R;
import dev.sieber.gifs.fragments.CategoryFragment;
import dev.sieber.gifs.fragments.TrendingFragment;
import dev.sieber.gifs.ui.favorites.view.FavouriteFragment;
import dev.sieber.gifs.ui.preferences.PrefsFragment;
import dev.sieber.gifs.utils.AdUtils;
import hotchemi.android.rate.AppRate;

public class MainActivity extends PurchaseActivity {

    private static final String BACK_STACK_ROOT_TAG = "root_fragment";

    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        bottomNavigationView.setOnNavigationItemSelectedListener(item -> {

            final Fragment fragment;
            switch (item.getItemId()) {
                case R.id.action_favorites:
                    fragment = FavouriteFragment.newInstance();
                    break;
                case R.id.action_trending:
                    fragment = TrendingFragment.newInstance();
                    break;
                case R.id.action_categories:
                    fragment = CategoryFragment.newInstance();
                    break;
                case R.id.action_settings:
                    fragment = new PrefsFragment();
                    break;
                default:
                    fragment = FavouriteFragment.newInstance();
                    break;
            }

            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragmentContainer, fragment)
                    .addToBackStack(BACK_STACK_ROOT_TAG)
                    .commit();

            return true;
        });
        bottomNavigationView.setSelectedItemId(R.id.action_trending);

        showRating();

        MobileAds.initialize(this, getString(R.string.ad_app_id));
        AdUtils.getInstance().showAdBanner(findViewById(R.id.adView));
    }

    @Override
    protected void onResume() {
        super.onResume();
        new File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString() + "/gifs/" + GifDetailActivity
                        .WHATS_APP_GIF_NAME
        ).delete();
    }

    /**
     * Add a fragment on top of the current tab
     */
    public void addFragmentOnTop(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragmentContainer, fragment)
                .addToBackStack(null)
                .commit();
    }

    private void showRating() {
        AppRate.with(this)
                .setInstallDays(1)
                .setLaunchTimes(4)
                .setRemindInterval(1)
                .setShowLaterButton(true)
                .monitor();

        // Show a dialog if meets conditions
        AppRate.showRateDialogIfMeetsConditions(this);
    }
}
