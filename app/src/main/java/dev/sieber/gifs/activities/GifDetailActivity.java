package dev.sieber.gifs.activities;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.koushikdutta.ion.Ion;
import com.varunest.sparkbutton.SparkButton;
import com.varunest.sparkbutton.SparkEventListener;

import java.io.File;
import java.io.FileOutputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dev.sieber.gifs.BuildConfig;
import dev.sieber.gifs.R;
import dev.sieber.gifs.adapter.GifAdapter;
import dev.sieber.gifs.model.GifImages;
import dev.sieber.gifs.model.Image;
import dev.sieber.gifs.model.Result;
import dev.sieber.gifs.net.TenorRetrofitBuilder;
import dev.sieber.gifs.utils.AdUtils;
import dev.sieber.gifs.utils.DeviceUtils;
import dev.sieber.gifs.utils.FavouriteUtils;
import dev.sieber.gifs.utils.GifUtils;
import dev.sieber.gifs.utils.ObjectCache;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GifDetailActivity extends PurchaseActivity implements Callback<GifImages>, GifAdapter.OnClickListener {

    public static final String WHATS_APP_GIF_NAME = "whats_app.gif";
    public static final String IMAGE_DATA = "image";
    private static int PERMISSION_REQUEST_CODE = 0;
    @BindView(R.id.detail_imageview)
    ImageView mImageView;
    @BindView(R.id.tv_user_display_name)
    TextView mUserDisplayName;
    @BindView(R.id.tv_gif_source)
    TextView mGifSource;
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.favourite_btn)
    SparkButton favouriteBtn;
    @BindView(R.id.share_btn)
    SparkButton shareBtn;
    @BindView(R.id.whatsapp_btn)
    SparkButton whatsAppBtn;
    @BindView(R.id.layout_progess)
    RelativeLayout progressView;
    @BindView(R.id.related_container)
    View relatedContainer;

    private GifAdapter mGifAdapter;
    private Result mImageData;

    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gif_detail);
        ButterKnife.bind(this);

        mImageData = (Result) ObjectCache.getInstance().getObject(IMAGE_DATA);

        if (mImageData == null || mImageData.getMedia() == null || mImageData.getMedia().size() == 0 || mImageData.getMedia().get(0) == null ||
                mImageData.getMedia()
                .get(0).getTinygif() == null) {
            finish();
        }

        final Image image = mImageData.getMedia().get(0).getTinygif();
        final int deviceWidth = DeviceUtils.getDeviceWidth(this);
        final int height = image.getDims()[1] * deviceWidth / image.getDims()[0];
        mImageView.setLayoutParams(new RelativeLayout.LayoutParams(deviceWidth, height));

        if (mImageData.getTitle() != null) {
            mUserDisplayName.setText(mImageData.getTitle());
        } else {
            mUserDisplayName.setVisibility(View.GONE);
            mGifSource.setVisibility(View.GONE);
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        favouriteBtn.setEventListener((button, buttonState) -> {
            if (checkNeededPermissions()) {
                if (buttonState) {
                    logEvent("save_favorite");
                    AdUtils.getInstance().displayFullScreenAd(GifDetailActivity.this);
                    new SaveTask().execute();
                } else {
                    logEvent("delete_favorite");
                    new DeleteTask().execute();
                }
            } else {
                favouriteBtn.setChecked(false);
            }
        });
        favouriteBtn.setEnabled(false);
        favouriteBtn.setChecked(FavouriteUtils.isFavourite(mImageData.getId()));

        whatsAppBtn.setOnClickListener(view -> {
            if (checkNeededPermissions()) {
                mProgressDialog = ProgressDialog.show(GifDetailActivity.this, "", "", true, false);
                logEvent("share_via_whatsapp");
                new WhatAppTask().execute();
            }
        });
        whatsAppBtn.setEnabled(false);
        if (!DeviceUtils.isAppInstalled(this, "com.whatsapp")) {
            whatsAppBtn.setAlpha(0);
        }

        shareBtn.setOnClickListener(view -> {
            logEvent("share");
            final Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_TEXT, mImageData.getUrl());
            startActivity(Intent.createChooser(shareIntent, "Share link using"));
        });
        shareBtn.setEnabled(false);

        GifUtils.loadGif(image.getUrl(), mImageView, (e, result) -> {
            favouriteBtn.setEnabled(true);
            whatsAppBtn.setEnabled(true);
            shareBtn.setEnabled(true);
            progressView.setVisibility(View.GONE);
        });
        checkNeededPermissions();

        if (mImageData.getTitle() != null && mImageData.getTitle().length() > 0) {
            final Call<GifImages> searchGifs = TenorRetrofitBuilder.getTenorService(this).getSearchGifs(mImageData.getTitle(), "30", "0", DeviceUtils
                    .getLanguageCode(), BuildConfig.api_key);
            searchGifs.enqueue(this);

            mRecyclerView.setHasFixedSize(true);
            mRecyclerView.setNestedScrollingEnabled(false);

            final StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, 1);
            mRecyclerView.setLayoutManager(staggeredGridLayoutManager);

            mGifAdapter = new GifAdapter();
            mGifAdapter.setOnClickListener(this);
            mRecyclerView.setAdapter(mGifAdapter);
        } else {
            relatedContainer.setVisibility(View.GONE);
        }

        AdUtils.getInstance().showAdBanner((AdView) findViewById(R.id.adView));
        AdUtils.getInstance().loadFullscreenAd(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString() + "/gifs/" + WHATS_APP_GIF_NAME).delete();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.tv_gif_source)
    public void onClickGifSource() {
        final Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(mImageData.getUrl()));
        startActivity(browserIntent);
    }

    private boolean checkNeededPermissions() {
        final boolean hasPermission = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager
                .PERMISSION_GRANTED;
        if (hasPermission) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
        }
        return !hasPermission;
    }

    @Override
    public void onResponse(Call<GifImages> call, Response<GifImages> response) {
        mGifAdapter.setData(response.body().getResults());
    }

    @Override
    public void onFailure(Call<GifImages> call, Throwable t) {
    }

    @Override
    public void onClickImage(int position) {
        ObjectCache.getInstance().putObject(GifDetailActivity.IMAGE_DATA, mGifAdapter.getItem(position));
        final Intent intent = new Intent(this, GifDetailActivity.class);
        startActivity(intent);
    }

    private void logEvent(final String eventName) {
        final FirebaseAnalytics firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        final Bundle bundle = new Bundle();
        firebaseAnalytics.logEvent(eventName, bundle);
    }

    class SaveTask extends AsyncTask<Void, Void, Void> {

        protected Void doInBackground(Void... urls) {
            FavouriteUtils.save(mImageData.getId(), mImageData.getMedia().get(0).getGif());
            return null;
        }
    }

    class DeleteTask extends AsyncTask<Void, Void, Void> {

        protected Void doInBackground(Void... urls) {
            FavouriteUtils.delete(mImageData.getId());
            return null;
        }
    }

    class WhatAppTask extends AsyncTask<Void, Void, Void> {

        protected Void doInBackground(Void... urls) {
            Ion.with(getBaseContext())
                    .load(mImageData.getMedia().get(0).getGif().getUrl())
                    .asInputStream()
                    .setCallback((e, result) -> {
                        try {
                            final File gifsRoot = new File(getFilesDir(), "/gifs");
                            if (!gifsRoot.exists()) {
                                gifsRoot.mkdir();
                            }

                            final File whatsAppFile = new File(gifsRoot, WHATS_APP_GIF_NAME);

                            final FileOutputStream fileOutput = new FileOutputStream(whatsAppFile);

                            final byte[] buffer = new byte[1024];
                            int bufferLength; //used to store a temporary size of the buffer

                            //now, read through the input buffer and write the contents to the file
                            while ((bufferLength = result.read(buffer)) > 0) {
                                //add the results in the buffer to the file in the file output stream (the file on the sd card
                                fileOutput.write(buffer, 0, bufferLength);
                            }
                            //close the output stream when done
                            fileOutput.close();

                            final Activity activity = GifDetailActivity.this;
                            final String provider = activity.getPackageName() + ".fileprovider";
                            final Uri photoURI = FileProvider.getUriForFile(activity, provider, whatsAppFile);

                            sendWhatsAppIntent(photoURI);
                        } catch (Exception de) {
                            de.printStackTrace();
                        }
                    });
            return null;
        }

        private void sendWhatsAppIntent(final Uri photoUri) {
            final Intent whatsAppIntent = new Intent(Intent.ACTION_SEND);
            whatsAppIntent.setType("image/gif");
            whatsAppIntent.setPackage("com.whatsapp");
            whatsAppIntent.putExtra(Intent.EXTRA_STREAM, photoUri);
            whatsAppIntent.putExtra(Intent.EXTRA_TEXT, "Send with GIFs for WhatsApp and Facebook\nhttps://play.google.com/store/apps/details?id=de" +
                    ".gifs");
            whatsAppIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivity(Intent.createChooser(whatsAppIntent, "Share with"));

            GifDetailActivity.this.runOnUiThread(() -> mProgressDialog.dismiss());
        }
    }
}
