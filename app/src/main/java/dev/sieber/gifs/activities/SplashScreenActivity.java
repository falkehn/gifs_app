package dev.sieber.gifs.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import dev.sieber.gifs.R;
import dev.sieber.gifs.model.GifImages;

public class SplashScreenActivity extends AppCompatActivity {

    @BindView(R.id.loading)
    TextView loading;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_progress);
        ButterKnife.bind(this);

        final Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.getString("key") != null) {
            final String searchKey = bundle.getString("key");
//            ObjectCache.getInstance().putObject(SearchActivity.SEARCH_KEY, searchKey);
//            final Intent searchIntent = new Intent(this, SearchActivity.class);
//            startActivity(searchIntent);
            finish();
        } else {
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }

        final Typeface customFont = Typeface.createFromAsset(getAssets(), "nevis.ttf");
        loading.setTypeface(customFont);
    }
}
